package com.chkris.creational.builder.car;

public class Engineer {

	protected CarBuilderInterface carBuilder;

	public void setBuilder(CarBuilderInterface carBuilder) {
		this.carBuilder = carBuilder;
	}

	public CarInterface buildCar() {
		carBuilder.buildBody().buildEngine().buildWheels();

		return carBuilder.getCar();
	}
}
