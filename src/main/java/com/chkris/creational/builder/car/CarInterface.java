package com.chkris.creational.builder.car;

public interface CarInterface {

	void setEngine(String name, int price);

	void setWheels(String name, int price);

	void setBody(String name, int price);

	public int getEnginePrice();

	public int getBodyPrice();

	public int getWheelsPrice();
}
