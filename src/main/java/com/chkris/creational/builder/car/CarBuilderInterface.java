package com.chkris.creational.builder.car;

public interface CarBuilderInterface {

	CarBuilderInterface buildEngine();

	CarBuilderInterface buildWheels();

	CarBuilderInterface buildBody();

	CarInterface getCar();
}
