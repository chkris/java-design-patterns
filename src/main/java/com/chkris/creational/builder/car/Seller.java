package com.chkris.creational.builder.car;

public class Seller {

	int getPrice(CarInterface car) {
		return car.getBodyPrice() + car.getEnginePrice() + car.getWheelsPrice();
	}
}
