package com.chkris.creational.builder.car;

public class Car implements CarInterface {

	protected String engine;
	protected String wheels;
	protected String body;

	protected int enginePrice;
	protected int bodyPrice;
	protected int wheelsPrice;

	public void setEngine(String name, int price) {
		this.engine = name;
		this.enginePrice = price;
	}

	public void setWheels(String name, int price) {
		this.wheels = name;
		this.wheelsPrice = price;
	}

	public void setBody(String name, int price) {
		this.body = name;
		this.bodyPrice = price;
	}
	
	public int getEnginePrice() {
		return enginePrice;
	}

	public int getBodyPrice() {
		return bodyPrice;
	}

	public int getWheelsPrice() {
		return wheelsPrice;
	}
}
