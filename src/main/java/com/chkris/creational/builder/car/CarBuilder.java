package com.chkris.creational.builder.car;

public class CarBuilder implements CarBuilderInterface {

	protected CarInterface car;

	public CarBuilder() {
		car = new Car();
	}

	public CarBuilderInterface buildEngine() {
		car.setEngine("v8", 1350);

		return this;
	}

	public CarBuilderInterface buildWheels() {
		car.setWheels("4x4", 450);

		return this;
	}

	public CarBuilderInterface buildBody() {
		car.setBody("ferrari", 600);

		return this;
	}

	public CarInterface getCar() {
		return car;
	}
}
