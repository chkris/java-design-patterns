
========================
Builder Pattern
========================

GOF definition
==============

Separate the construction of complex objects from its representation so that the same construction process can create different representations.

Participants
============

* Builder:
	- define an interface for building/creating parts of Products object
* ConcreteBuilder: 
	- constructs and assembles parts of products by implementing Builder interface.
	- defines and keep track of Product its creates
	- provides an interface for retrieving products
* Director:
	- construct an object using Builder interface
* Product:
	- represent the complex under construction
	- ConcreteBuilder builds the Product internal representation and defines the process by witch its assembled
	- includes the classes that defines the constituent parts, including interfaces for assembling parts into the final result 