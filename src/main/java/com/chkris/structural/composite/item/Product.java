package com.chkris.structural.composite.item;

public class Product implements Item {

	protected String description;
	protected Double price;

	public Product(String description, Double price) {
		this.description = description;
		this.price = price;
	}

	public void add(Item item) {
		throw new UnsupportedOperationException(
				"Can't add to Product object of type Item");
	}

	public Double getPrice() {
		return this.price;
	}

}
