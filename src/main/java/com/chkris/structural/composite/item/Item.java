package com.chkris.structural.composite.item;

public interface Item {

	void add(Item item);

	Double getPrice();
}
