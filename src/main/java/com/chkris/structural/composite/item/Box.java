package com.chkris.structural.composite.item;

import java.util.ArrayList;
import java.util.List;

public class Box implements Item {

	protected String name;
	protected Double price;
	protected List<Item> itemList;

	public Box(String name, Double price) {
		this.name = name;
		this.price = price;
		this.itemList = new ArrayList<>();
	}

	public void add(Item item) {
		itemList.add(item);
	}

	public Double getPrice() {
		Double totalPrice = price;
		
		for (Item item : itemList) {
			totalPrice += item.getPrice();
		}
		
		return totalPrice;
	}

}
