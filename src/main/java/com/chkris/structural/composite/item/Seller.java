package com.chkris.structural.composite.item;

public class Seller {

	public Double calculatePrice(Item item) {
		return item.getPrice();
	}
}
