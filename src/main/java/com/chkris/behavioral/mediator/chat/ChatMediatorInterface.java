package com.chkris.behavioral.mediator.chat;

public interface ChatMediatorInterface {

	void sendMessage(String message, UserInterface user);

	void addUser(UserInterface user);
}
