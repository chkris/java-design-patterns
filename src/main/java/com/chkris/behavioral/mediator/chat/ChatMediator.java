package com.chkris.behavioral.mediator.chat;

import java.util.ArrayList;
import java.util.List;

public class ChatMediator implements ChatMediatorInterface {

	protected List<UserInterface> userList;

	public ChatMediator() {
		this.userList = new ArrayList<>();
	}

	public void addUser(UserInterface user) {
		this.userList.add(user);
	}

	/**
	 * @param String message
	 * @param UserInteface currentUser
	 */
	public void sendMessage(String message, UserInterface currentUser) {

		for (UserInterface user : this.userList) {
			if (user != currentUser) {
				user.recive(message);
			}
		}

	}
}
