package com.chkris.behavioral.mediator.chat;

public class MessageLogger {

	protected String log = "";

	public void log(String log) {
		this.log += log;
	}

	public String getLog() {
		return this.log;
	}
}
