package com.chkris.behavioral.mediator.chat;

public class User implements UserInterface {

	protected ChatMediatorInterface chatMediator;
	protected String name;
	protected MessageLogger messageLogger;

	public User(ChatMediatorInterface chatMediator, String name,
			MessageLogger messageLogger) {
		this.chatMediator = chatMediator;
		this.name = name;
		this.messageLogger = messageLogger;
	}

	public void send(String message) {
		String compundMessage = "User " + this.name + " send " + message;
		this.messageLogger.log(compundMessage);
		this.chatMediator.sendMessage(message, this);
	}

	public void recive(String message) {
		String compundMessage = "User " + this.name + " receive " + message;
		this.messageLogger.log(compundMessage);
	}
}
