
========================
Mediator Pattern
========================

GOF definition
==============

Define an object that encapsulates how a set of objects interact. 
Mediator promotes loose coupling by keeping objects from referring to each other explicitly,
and it let you vary their interaction independently. 

Applicability
=============

+ A set of objects communicates in well defined but complex ways. The resulting interdependencies are unstructured and difficult to understand.
+ Reusing objects is difficult because it refers to and communicate with many others objects.
+ A behavior that is distributed between several classes should be customizable without a lot of subclassing. 

Intent
======

As program grows in complexity there is hard to maintain relation and communication in many-to-many 
or many-to-one relationship between objects. The Mediator design pattern attempts to centralize the
channels of communication across application or related units of functionalities. This reduce complexity
and maintainability.  

Where it can be applied 
=======================

+ Chat - each user communicate through single point the ChatMediator not to each other
+ Air traffic control - each plane communicates with ATC
+ File system groups
+ Stock brokers - stock exchange
+ Graphical user interface - encapsulate behavior of related elements. 
  Pressing a button can enhance the element on the list where communication between many buttons and list or other elements can lead to unmaintainability      