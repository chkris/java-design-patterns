package com.chkris.behavioral.mediator.stock;

public class AmericanSeller {

	private Mediator mediator;

	private static final double acceptedBid = 10;

	public AmericanSeller(Mediator mediator) {
		this.mediator = mediator;
		this.mediator.registerAmericanSeller(this);
	}

	public boolean isBidAccepted(double amount) {
		return amount > acceptedBid ? true : false;
	}

}
