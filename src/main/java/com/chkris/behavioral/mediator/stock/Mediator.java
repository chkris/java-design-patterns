package com.chkris.behavioral.mediator.stock;

public class Mediator {

	protected DollarConverter dollarConverter;
	protected AmericanSeller americanSeller;

	private static int numberOfAttempts = 0;

	public boolean placeBid(double amount, String currency)
			throws DollarConverterUnsuportedArgumentException {
		numberOfAttempts++;

		return americanSeller.isBidAccepted(dollarConverter.convert(currency,
				amount));
	}

	public void registerAmericanSeller(AmericanSeller americanSeller) {
		this.americanSeller = americanSeller;
	}

	public void registerDollarConverter(DollarConverter dollarConverter) {
		this.dollarConverter = dollarConverter;
	}

	public int getNumberOfAttempts() {
		return numberOfAttempts;
	}
}
