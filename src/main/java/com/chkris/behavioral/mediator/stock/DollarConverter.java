package com.chkris.behavioral.mediator.stock;

public class DollarConverter {

	private static final double DOLLAR_UNIT = 1.0;
	private static final double YEN_UNIT = 0.01;
	private static final double EURO_UNIT = 1.31;

	public static final String YEN_NAME = "yen";
	public static final String EURO_NAME = "euro";

	private Mediator mediator;

	public DollarConverter(Mediator mediator) {
		this.mediator = mediator;
		this.mediator.registerDollarConverter(this);
	}

	public double convertEuroToDollar(double euro) {
		return euro * (EURO_UNIT / DOLLAR_UNIT);
	}

	public double convertYenToDollar(double yen) {
		return yen * (YEN_UNIT / EURO_UNIT);
	}

	public double convert(String currency, double amount)
			throws DollarConverterUnsuportedArgumentException {
		switch (currency) {
		case YEN_NAME:
			return convertYenToDollar(amount);
		case EURO_NAME:
			return convertEuroToDollar(amount);
		default:
			throw new DollarConverterUnsuportedArgumentException();
		}
	}

}
