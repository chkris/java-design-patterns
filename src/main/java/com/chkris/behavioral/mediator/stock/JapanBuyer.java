package com.chkris.behavioral.mediator.stock;

public class JapanBuyer extends Buyer {

	public JapanBuyer(Mediator mediator) {
		super(mediator, DollarConverter.YEN_NAME);
	}
}
