package com.chkris.behavioral.mediator.stock;

public class Buyer {

	Mediator mediator;
	String currency;

	public Buyer(Mediator mediator, String currency) {
		this.mediator = mediator;
		this.currency = currency;
	}

	public boolean attemptToPurchase(double bid)
			throws DollarConverterUnsuportedArgumentException {
		return mediator.placeBid(bid, currency);
	}
}
