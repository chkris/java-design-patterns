package com.chkris.behavioral.mediator.stock;

public class GermanBuyer extends Buyer {

	public GermanBuyer(Mediator mediator) {
		super(mediator, DollarConverter.EURO_NAME);
	}
}
