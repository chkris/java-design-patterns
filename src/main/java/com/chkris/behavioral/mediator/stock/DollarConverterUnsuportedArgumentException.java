package com.chkris.behavioral.mediator.stock;

public class DollarConverterUnsuportedArgumentException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String EXCEPTION_MESSAGE = "There is no such currency name";
	
	public DollarConverterUnsuportedArgumentException()
	{
		super(EXCEPTION_MESSAGE);		
	}
}
