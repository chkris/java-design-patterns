package com.chkris.structural.composite.item;

import junit.framework.Assert;
import junit.framework.TestCase;

public class ItemCompositeTest extends TestCase {

	public void testStockExchange() {
		
		Item hugeBox = new Box("huge box", 10.0);
		
		Item iPhone = new Product("iPhone", 400.0);
		Item samsung = new Product("samsung", 350.0);
		Item nokia = new Product("nokia", 200.0);
		
		Item boxForBatteries = new Box("box for batteries", 5.0);
		
		Item iPhoneBattery = new Product("iPhone battery", 50.0);
		Item samsungBattery = new Product("samsung battery", 30.0);
		Item nokiaBattery = new Product("nokia battery", 20.0);
				
		hugeBox.add(iPhone);
		hugeBox.add(samsung);
		hugeBox.add(nokia);		
		hugeBox.add(boxForBatteries);
		
		boxForBatteries.add(iPhoneBattery);
		boxForBatteries.add(samsungBattery);
		boxForBatteries.add(nokiaBattery);
		
		Seller seller = new Seller();
				
		Assert.assertEquals(1065.0, seller.calculatePrice(hugeBox));
		Assert.assertEquals(400.0, seller.calculatePrice(iPhone));
		Assert.assertEquals(105.0, seller.calculatePrice(boxForBatteries));
	}
}
