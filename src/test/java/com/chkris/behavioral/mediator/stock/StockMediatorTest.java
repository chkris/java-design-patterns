package com.chkris.behavioral.mediator.stock;

import junit.framework.Assert;
import junit.framework.TestCase;

public class StockMediatorTest extends TestCase {

	public void testStockExchange()
			throws DollarConverterUnsuportedArgumentException {

		Mediator mediator = new Mediator();

		new DollarConverter(mediator);
		new AmericanSeller(mediator);

		Buyer japanBuyer = new JapanBuyer(mediator);
		Buyer germanBuyer = new GermanBuyer(mediator);

		double yen = 0.0;
		while (!japanBuyer.attemptToPurchase(yen)) {
			yen += 15.0;
		}
		Assert.assertEquals(89, mediator.getNumberOfAttempts());

		double euro = 0.0;
		while (!germanBuyer.attemptToPurchase(euro)) {
			euro += 3.0;
		}
		Assert.assertEquals(93, mediator.getNumberOfAttempts());
	}
}
