package com.chkris.behavioral.mediator.chat;

import junit.framework.Assert;
import junit.framework.TestCase;

public class ChatMediatorTest extends TestCase {

	public void testChat() {

		MessageLogger messageLogger = new MessageLogger();
		ChatMediatorInterface chatMediator = new ChatMediator();

		UserInterface userOne = new User(chatMediator, "Chris", messageLogger);
		UserInterface userTwo = new User(chatMediator, "Mark", messageLogger);
		UserInterface userThree = new User(chatMediator, "Julia", messageLogger);

		chatMediator.addUser(userOne);
		chatMediator.addUser(userTwo);
		chatMediator.addUser(userThree);

		userOne.send("Hello All");

		Assert.assertEquals("User Chris send Hello All"
				+ "User Mark receive Hello All"
				+ "User Julia receive Hello All", messageLogger.getLog());
	}
}
