package com.chkris.creational.builder.car;

import junit.framework.Assert;
import junit.framework.TestCase;

public class CarBuilderTest extends TestCase {

	public void testCarBuilder() {

		Engineer engineer = new Engineer();
		CarBuilder carBuilder = new CarBuilder();

		engineer.setBuilder(carBuilder);

		CarInterface car = engineer.buildCar();

		Seller seller = new Seller();

		Assert.assertEquals(2400, seller.getPrice(car));
	}
}